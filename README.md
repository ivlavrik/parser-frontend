# parser-frontend
## Quick Start

- Install [Node](https://nodejs.org/en/) 6.9.1 (latest working version) (the way is dependent on your preferences and OS)

- To install dependencies run from working directory

### `npm install`

- Start development server:

### `npm start`

- Visit `localhost:3000`

### `npm start`

- Don't forget to start API application (see instructions in correspondent repository)
