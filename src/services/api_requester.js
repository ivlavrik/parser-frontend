import Axios from 'axios';

const axios = Axios.create({
  baseURL: '/api/v1',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  },
  responseType: 'json'
});

export function get(url, data = {}, options = {}) {
  return axios.get(url, { params: data, ...options });
}

export function post(url, data = {}, options = {}) {
  return axios.post(url, data, options);
}

export function put(url, data = {}, options = {}) {
  return axios.put(url, data, options);
}

export function patch(url, data = {}, options = {}) {
  return axios.patch(url, data, options);
}

export function destroy(url, data = {}, options = {}) {
  return axios.delete(url, data, options);
}
