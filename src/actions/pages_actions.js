import {
  GET_LIST_REQUEST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILURE,
  PARSE_PAGE_REQUEST,
  PARSE_PAGE_SUCCESS,
  PARSE_PAGE_FAILURE,
  CHANGE_URL
} from '../constants/pages_list';

import { get, post } from '../services/api_requester';

export function getList() {
  return( dispatch => {
    dispatch({ type: GET_LIST_REQUEST });
    get('/pages').then(response => {
      dispatch({
        type: GET_LIST_SUCCESS,
        payload: response.data
      })
    }).catch(() => {
      dispatch({ type: GET_LIST_FAILURE });
    });
  });
}

export function changeUrl(url) {
  return{
    type: CHANGE_URL,
    payload: url
  }
}

export function parsePage(url) {
  return( dispatch => {
    dispatch({ type: PARSE_PAGE_REQUEST });
    post('/pages/parse', {url: url}).then(response => {
      dispatch({
        type: PARSE_PAGE_SUCCESS,
        payload: response.data
      })
    }).catch(() => {
      dispatch({ type: PARSE_PAGE_FAILURE });
    });
  });
}
