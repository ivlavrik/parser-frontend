import {
  GET_LIST_REQUEST,
  GET_LIST_SUCCESS,
  GET_LIST_FAILURE,
  PARSE_PAGE_REQUEST,
  PARSE_PAGE_SUCCESS,
  PARSE_PAGE_FAILURE,
  CHANGE_URL
} from '../constants/pages_list';

import { unionBy } from 'lodash';

const initialState = {
  url: '',
  items: [],
  error: null
}

export default function(state = initialState, action) {
  switch(action.type) {
    case GET_LIST_REQUEST:
      return { ...state, fetching: true, error: null}
    case GET_LIST_SUCCESS:
      return { ...state, fetching: false, items: action.payload }
    case GET_LIST_FAILURE:
      return { ...state, fetching: false, error: 'Error while loading list' }
    case PARSE_PAGE_REQUEST:
      return { ...state, fetching: true, error: null}
    case PARSE_PAGE_SUCCESS:
      return { ...state, fetching: false, items: unionBy(state.items, [action.payload], 'id') }
    case PARSE_PAGE_FAILURE:
      return { ...state, fetching: false, error: 'Error while parsing page' }
    case CHANGE_URL:
      return { ...state, url: action.payload }
    default:
      return state;
  }
}
