import { combineReducers } from 'redux';
import pagesList from './pages_list';

export default combineReducers({
  pagesList
})
