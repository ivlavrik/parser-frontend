import React, { Component } from 'react';
import AttributeList from './AttributeList';

export default class Tag extends Component {
  render() {
    const item = this.props.item;
    return (
      <span>
        <dt>{item.key}</dt>
        <dd>{item.value}</dd>
        <dd>
          <AttributeList items={item.tag_attributes} />
        </dd>
      </span>
    );
  }
}
