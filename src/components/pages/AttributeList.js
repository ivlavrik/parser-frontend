import React, { Component } from 'react';
import Attribute from './Attribute';

export default class AttributeList extends Component {
  render() {
    return (
      <ul>
        {
          this.props.items.map(item => {
            return <Attribute key={`attribute_list_item_${item.id}`} item={item} />
          })
        }
      </ul>
    );
  }
}
