import React, { Component } from 'react';
import { Link } from 'react-router';
import TagList from './TagList';

export default class Item extends Component {
  render() {
    const item = this.props.item;
    return (
      <tbody>
        <tr>
          <td className="text-center">{item.id}</td>
          <td>
            <Link to={item.url}>{item.url}</Link>
            <TagList items={item.tags} />
          </td>
        </tr>
      </tbody>
    );
  }
}
