import React, { Component } from 'react';

export default class Attribute extends Component {
  render() {
    const item = this.props.item;
    return (
      <li>
        <strong>{item.key}</strong>: {item.value}
      </li>
    );
  }
}
