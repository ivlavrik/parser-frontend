import React, { Component } from 'react';
import { FormGroup, FormControl, InputGroup, Button } from 'react-bootstrap';

export default class ParseBox extends Component {
  render() {
    const { url, onChangeUrl, onParse } = this.props;
    const btnClassName = url && !!url.trim() ? '' : 'disabled';
    return (
      <FormGroup>
        <InputGroup>
          <FormControl type="text" onChange={onChangeUrl}/>
          <InputGroup.Button>
            <Button className={btnClassName} onClick={onParse}>Parse!</Button>
          </InputGroup.Button>
        </InputGroup>
      </FormGroup>
    );
  }
}
