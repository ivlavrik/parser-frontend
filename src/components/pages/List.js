import React, { Component } from 'react';
import Item from './Item';

export default class List extends Component {
  render() {
    return (
      <table className="table table-bordered">
        <thead>
          <tr>
            <th className="text-center">id</th>
            <th className="text-center">url</th>
          </tr>
        </thead>
        {
          this.props.items.map(item => {
            return <Item key={`pages_list_item_${item.id}`} item={item} />
          })
        }
      </table>
    );
  }
}
