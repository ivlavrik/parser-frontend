import React, { Component } from 'react';
import Tag from './Tag';

export default class TagList extends Component {
  render() {
    return (
      <dl className="dl-horizontal">
        {
          this.props.items.map(item => {
            return <Tag key={`tag_list_item_${item.id}`} item={item} />
          })
        }
      </dl>
    );
  }
}
