import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

export default class ErrorMessage extends Component {
  render() {
    return (
      <Row className="show-grid">
        <Col md={12} className="alert alert-danger text-center">
          Uh-oh, something went wrong.
        </Col>
      </Row>
    )
  }
}
