import React from 'react';
import { Route, IndexRedirect } from 'react-router';

import App from './containers/app';
import PagesList from './containers/pages_list';
import ErrorMessage from './components/error';

const routes = (
  <div>
    <Route path='/' component={App}>
      <IndexRedirect to='pages' />
      <Route path="pages" component={ PagesList } />
    </Route>
    <Route path='*' component={ErrorMessage} />
  </div>
)

export default routes;
