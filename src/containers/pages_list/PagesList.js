import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header, List, ParseBox } from '../../components/pages';
import ErrorMessage from '../../components/error';
import * as pagesActions from  '../../actions/pages_actions';

class PagesListPage extends Component {
  componentDidMount() {
    this.props.pagesActions.getList();
  }

  handleChangeUrl(e) {
    const { changeUrl } = this.props.pagesActions;
    changeUrl(e.target.value);
  }

  handleParse() {
    const { url } = this.props.pagesList;
    const { parsePage } = this.props.pagesActions;
    parsePage(url);
  }

  render() {
    const { items, fetching, error, url } = this.props.pagesList;
    return (
      <div>
        <Header />
        <List items={items} />
        <ParseBox
          url={url}
          onChangeUrl={this.handleChangeUrl.bind(this)}
          onParse={this.handleParse.bind(this)}
        />
        { fetching ? 'Processing...' : '' }
        { error ? <ErrorMessage /> : '' }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    pagesActions: bindActionCreators(pagesActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PagesListPage);
