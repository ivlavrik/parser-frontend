import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from 'react-bootstrap';

class App extends Component {
  render() {
    return (
      <Grid>
        {this.props.children}
      </Grid>
    );
  }
}

export default connect(x => x)(App);
